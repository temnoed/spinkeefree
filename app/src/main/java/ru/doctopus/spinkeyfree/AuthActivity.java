package ru.doctopus.spinkeyfree;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.Arrays;
import java.util.List;

import ru.doctopus.spinkeefree.R;

public class AuthActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "GoogleActivity";
    private static final int RC_SIGN_IN = 9001;
    private ProgressDialog authLoading;

    private FirebaseAuth mAuth;

    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager mCallbackManager;
    private List<String> permissionNeeds = Arrays.asList("email", "public_profile");

    private EditText emailField;
    private EditText passwordField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_auth);
        mAuth = FirebaseAuth.getInstance();

        emailField = findViewById(R.id.emailField);
        passwordField = findViewById(R.id.passwordField);
        emailField.setOnClickListener(this);
        passwordField.setOnClickListener(this);
        findViewById(R.id.fbButton).setOnClickListener(this);
        findViewById(R.id.authButton).setOnClickListener(this);
        findViewById(R.id.googleButton).setOnClickListener(this);
        findViewById(R.id.newUser).setOnClickListener(this);
        findViewById(R.id.forgotPassword).setOnClickListener(this);

        authLoading = new ProgressDialog(this);
        authLoading.setMessage(getString(R.string.please_wait));

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.oauth))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mCallbackManager = CallbackManager.Factory.create();
        /*loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });*/


        LoginButton loginButton = findViewById(R.id.fbLoginButton);
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.d(TAG, "facebook:onError", exception);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            startActivity(new Intent(getApplicationContext(), MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
        }
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        showProgressDialog();

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            checkSignIn(user);
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(AuthActivity.this, R.string.auth_error, Toast.LENGTH_SHORT).show();
                        }
                        hideProgressDialog();
                    }
                });
    }

    private void createAccount(String email, String password) {
        Log.d(TAG, "createAccount:" + email);
        showProgressDialog();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            checkSignIn(user);
                        } else {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        }

                        hideProgressDialog();
                    }
                });
    }

    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            checkSignIn(user);
                        } else {
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                        }
                        hideProgressDialog();
                    }
                });
    }

    private void sendEmailVerification() {
        //findViewById(R.id.verifyEmailButton).setEnabled(false);
        final FirebaseUser user = mAuth.getCurrentUser();
        user.sendEmailVerification()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //findViewById(R.id.verifyEmailButton).setEnabled(true);
                        if (task.isSuccessful()) {
                            checkSignIn(user);
                        } else {
                            Log.e(TAG, "sendEmailVerification", task.getException());
                        }
                    }
                });
    }

    private boolean validateForm() {
        boolean valid = true;

        if (TextUtils.isEmpty(emailField.getText().toString())) {
            emailField.setError(getString(R.string.enterEmail));
            valid = false;
        } else {
            emailField.setError(null);
        }

        if (TextUtils.isEmpty(passwordField.getText().toString())) {
            passwordField.setError(getString(R.string.enterPassword));
            valid = false;
        } else {
            passwordField.setError(null);
        }

        return valid;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                if (account != null) {
                    firebaseAuthWithGoogle(account);
                }
            } catch (ApiException e) {
                hideProgressDialog();
                Toast.makeText(AuthActivity.this, R.string.auth_error, Toast.LENGTH_SHORT).show();
                Log.w(TAG, "Google sign in failed", e);
            }
        }
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            checkSignIn(user);
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            // TODO fail auth
                        }
                    }
                });
    }

    private void checkSignIn(FirebaseUser user) {
        if (user != null) {
            startActivity(new Intent(getApplicationContext(), MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
        } else {
            hideProgressDialog();
            Toast.makeText(AuthActivity.this, R.string.auth_error, Toast.LENGTH_SHORT).show();
        }
    }

    private void signIn() {
        showProgressDialog();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void showProgressDialog() {
        if (authLoading != null) {
            authLoading.show();
        }
    }

    private void hideProgressDialog() {
        if (authLoading.isShowing() && authLoading != null) {
            authLoading.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.googleButton) {
            signIn();
        } else if (i == R.id.fbButton) {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
        } else if (i == R.id.authButton) {
            signIn(emailField.getText().toString(), passwordField.getText().toString());
        } else if (i == R.id.newUser) {
            View regDialog = getLayoutInflater().inflate(R.layout.new_user_dialog, null);
            final EditText regEmail = regDialog.findViewById(R.id.regEmail);
            final EditText regPassword = regDialog.findViewById(R.id.regPassword);
            final EditText regRepeatPassword = regDialog.findViewById(R.id.repeatRegPassword);
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.text_registration))
                    .setMessage(getString(R.string.text_newLogPass))
                    .setView(regDialog)
                    .setPositiveButton(getString(R.string.text_next), null)
                    .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).create();

            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
                    Button positive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    positive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d(TAG, "onClick: " + regEmail.getText().toString());
                            Log.d(TAG, "onClick: " + regPassword.getText().toString());
                            Log.d(TAG, "onClick: " + regRepeatPassword.getText().toString());

                            if (TextUtils.isEmpty(regEmail.getText().toString())) {
                                regEmail.setError(getString(R.string.fill_input_field));
                                return;
                            } else {
                                regEmail.setError(null);
                            }

                            if (TextUtils.isEmpty(regPassword.getText().toString()) || regPassword.getText().toString().length() < 6) {
                                regPassword.setError(getString(R.string.pass_max_lenth));
                                return;
                            } else {
                                regPassword.setError(null);
                            }

                            if (TextUtils.isEmpty(regRepeatPassword.getText().toString()) || regRepeatPassword.getText().toString().length() < 6) {
                                regRepeatPassword.setError(getString(R.string.pass_max_lenth));
                                return;
                            } else {
                                regRepeatPassword.setError(null);
                            }

                            if (!regPassword.getText().toString().equals(regRepeatPassword.getText().toString())) {
                                regPassword.setError(getString(R.string.pass_dont_match));
                                regRepeatPassword.setError(getString(R.string.pass_dont_match));
                                return;
                            } else {
                                regPassword.setError(null);
                                regRepeatPassword.setError(null);
                            }
                            createAccount(regEmail.getText().toString(), regPassword.getText().toString());
                            dialog.dismiss();
                        }
                    });
                }
            });
            dialog.show();
        } else if (i == R.id.forgotPassword) {
            View resetDialog = getLayoutInflater().inflate(R.layout.reset_dialog, null);
            final EditText resetEmail = resetDialog.findViewById(R.id.resetEmail);
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.password_recovery))
                    .setMessage(getString(R.string.enterEmail))
                    .setView(resetDialog)
                    .setPositiveButton(getString(R.string.text_next), null)
                    .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).create();

            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
                    Button positive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    positive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d(TAG, "onClick: " + resetEmail.getText().toString());

                            if (TextUtils.isEmpty(resetEmail.getText().toString())) {
                                resetEmail.setError(getString(R.string.fill_input_field));
                                return;
                            } else {
                                resetEmail.setError(null);
                            }

                            mAuth.sendPasswordResetEmail(resetEmail.getText().toString())
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(AuthActivity.this, getString(R.string.email_sent_to_email) + resetEmail, Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(AuthActivity.this, R.string.failed_send_email, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                            dialog.dismiss();
                        }
                    });
                }
            });
            dialog.show();
        }
    }
}